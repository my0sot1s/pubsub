package pubsub

import (
	"github.com/go-redis/redis"
	"gitlab.com/my0sot1s/helper"
)

// Cb is callback
type Cb = func(string, string)

const (
	// SUCCESS state
	SUCCESS = "success"
	// ERROR state
	ERROR = "error"
)

// PubSub is define ps
type PubSub struct {
	client    *redis.Client
	clientID  string
	sessionID string
	pubsub    *redis.PubSub
}

// // Message is define message
// type Message struct {
// 	Payload []byte `json:"payload"`
// 	Channel string `json:"channel"`
// 	State   string `json:"state"`
// }

// ErrorMSG is define message
type ErrorMSG struct {
	Err         error  `json:"err"`
	Description string `json:"description"`
}

// func createMessageByState(state string, payload []byte, channel string) *Message {
// 	msg := &Message{
// 		Payload: payload,
// 		Channel: channel,
// 	}
// 	if state == SUCCESS {
// 		msg.State = SUCCESS
// 	}
// 	if state == ERROR {
// 		msg.State = ERROR
// 	}
// 	return nil
// }

// InitPubSubClient init client
func (ps *PubSub) InitPubSubClient(addr, pw string) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pw,
		DB:       0,
	})
	ps.client = client
}

// SortClose only close
func (ps *PubSub) SortClose() {
	ps.client.Close()
}

// Publish message to channel
func (ps *PubSub) Publish(channel string, messages []byte) {
	err := ps.client.Publish(channel, string(messages)).Err()
	helper.ErrLog(err)
}

// HandlerAddSub add sublist
func (ps *PubSub) HandlerAddSub(channels []string) {
	ps.pubsub = ps.client.Subscribe(channels...)
}

// Subscribe message to channel
func (ps *PubSub) Subscribe(payload chan<- helper.MS) {
	// defer ps.SortClose()
	msg, err := ps.pubsub.ReceiveMessage()
	helper.ErrLog(err)
	prePayload := map[string]string{
		"channel": msg.Channel,
		"payload": msg.Payload,
	}
	payload <- prePayload
}

func (ps *PubSub) handleUnsubscribe(channels []string) {
	err := ps.pubsub.Unsubscribe(channels...)
	helper.ErrLog(err)
}

// HandlerRequest Like Request response
func (ps *PubSub) HandlerRequest(channel string, payload []byte) string {
	ps.HandlerAddSub([]string{channel + "_response"})
	ps.Publish(channel+"_request", payload)
	response := make(chan helper.MS)
	go ps.Subscribe(response)
	x := <-response
	go func() {
		ps.handleUnsubscribe([]string{channel + "_response"})
		ps.handleUnsubscribe([]string{channel + "_request"})
		ps.pubsub.Close()
	}()
	return x["payload"]
}
