package pubsub

import (
	"encoding/json"
	"log"
	"testing"

	"github.com/gorilla/websocket"
)

// Event just message have name = `subscribe` || `unsubscribe`
type Event struct {
	Name    string `json:"name"`
	PayLoad string `json:"payload"`
}

// Message is struct message
type Message struct {
	ID      string `json:"id"`
	Type    string `json:"type"`
	Text    string `json:"text"`
	Created int    `json:"created"`
	// By is author
	By string `json:"by"`
	// To is room
	To string `json:"to"`
}

func TestWs(t *testing.T) {
	// params
	url := "ws://localhost:8000/ws"
	subRoom := []string{
		"chan1",
		"room.$default",
	}
	// var clientID string
	c, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	bin, _ := json.Marshal(subRoom)
	subscribe := Event{
		Name:    "subscribe",
		PayLoad: string(bin),
	}
	bin, _ = json.Marshal(subscribe)
	// join Room
	err = c.WriteMessage(websocket.TextMessage, bin)
	if err != nil {
		log.Fatal("Can not join: ", err)
	}
	print("Sub success!!!")

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			log.Printf("recv: %s", message)
		}
	}()
	m := Message{
		Type: "text",
		Text: "Hello",
		To:   "room.$default",
	}
	bin, _ = json.Marshal(m)
	err = c.WriteMessage(websocket.TextMessage, bin)
	if err != nil {
		log.Println("write:", err)
		return
	}
	<-done
}
