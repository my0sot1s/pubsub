package pubsub

import (
	"os"
	"strings"
	"testing"

	"github.com/joho/godotenv"
	"gitlab.com/my0sot1s/helper"
)

func TestSubscribe(t *testing.T) {
	pubsub := &PubSub{}
	err := godotenv.Load(".env")
	helper.ErrLog(err)
	helper.Log(os.Getenv("DB_HOST") + os.Getenv("DB_PORT"))
	helper.Log(os.Getenv("DB_PASS"))
	pubsub.InitPubSubClient(os.Getenv("DB_HOST")+":"+os.Getenv("DB_PORT"), os.Getenv("DB_PASS"))
	// subscribe
	chans := []string{"chan1_request", "chan2_request", "chan3_request"}
	pubsub.HandlerAddSub(chans)
	for {
		m := make(chan helper.MS)
		go pubsub.Subscribe(m)
		msg := <-m
		requestChannel := strings.Replace(msg["channel"], "_request", "", -1)
		go access(pubsub, requestChannel, msg)
		close(m)
	}
}

func access(pubsub *PubSub, requestChannel string, msg helper.MS) {
	switch requestChannel {

	case "chan1":
		//
		helper.Log("vao chan1")
		payload := msg["payload"]
		pubsub.Publish("chan1_response", []byte(payload+"____hello___response1 done"))
	case "chan2":
		//
		helper.Log("vao chan2")
		payload := msg["payload"]
		pubsub.Publish("chan2_response", []byte(payload+"____hello___response2 done"))
	case "chan3":
		//
		helper.Log("vao chan3")
		payload := msg["payload"]
		pubsub.Publish("chan3_response", []byte(payload+"____hello___response3 done"))
	}
}

func TestPublish(t *testing.T) {
	pubsub := &PubSub{}
	err := godotenv.Load(".env")
	helper.ErrLog(err)

	pubsub.InitPubSubClient(os.Getenv("DB_HOST")+":"+os.Getenv("DB_PORT"), os.Getenv("DB_PASS"))
	pubsub.Publish("chan1", []byte("aaaa"))
}

func TestRequest(t *testing.T) {
	pubsub := &PubSub{}
	err := godotenv.Load(".env")
	helper.ErrLog(err)

	pubsub.InitPubSubClient(os.Getenv("DB_HOST")+":"+os.Getenv("DB_PORT"), os.Getenv("DB_PASS"))
	helper.Log(pubsub.HandlerRequest("chan4", []byte("aaaa")))
}
