const redis = require('redis')

class PubSub {
	constructor() {
		// create publisher and subscriber
		this.subscriber = null
		this.publisher = null
	}
	initPubSub(config, cb) {
		this.subscriber = redis.createClient(config)
		this.publisher = redis.createClient(config)
		cb()
	}
	handlerAddSubList(listChannel) {
		return new Promise(resolve => {
			this.subscriber.subscribe(listChannel, resolve)
		})

	}
	handlerSubsribe() {
		return new Promise(resolve => {
			this.subscriber.on("message", (channel, message) => {
				console.log(channel, message)
				resolve({ channel, message })
			})
		})

	}
	handlerUnsub(chan) {
		return new Promise(resolve => {
			resolve()
		})
	}

	handlerPublish(channel, payload) {
		if (typeof payload === 'object') payload = JSON.stringify(payload)

		this.publisher.publish(channel, payload)
	}
	request(channel, requestPayload) {
		// add subList
		return this.handlerAddSubList(channel + '_request')
			.then(() => {
				console.log("sub added: ", channel + '_request')
				return
			})
			.then(() => {
				this.handlerPublish(channel + '_request', requestPayload)
				console.log('published: ', requestPayload)
				return this.handlerSubsribe(channel + '_response')
			})
			.then(({ channel, message }) => {
				console.log('chan:', channel, 'message', message)
				return { channel, message }
			})
	}
}

module.exports = PubSub