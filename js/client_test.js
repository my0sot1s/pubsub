const PubSub  = require('./client')
const path = require('path')
require('dotenv').config({path:path.join(__dirname,'../.env')})
const {DB_PORT,DB_HOST, DB_PASS } = process.env

const config = {
	port      : DB_PORT,               // replace with your port
	host      : DB_HOST,// replace with your hostanme or IP address
	password  : DB_PASS,    // replace with your password
}

let ps = new PubSub()

ps.initPubSub(config, function() {
	console.log('ps client init done!!')
})

ps.request('chan1', 'tete').then( data =>{
	console.log('....data:', data)
	ps.handlerUnsub()
})
