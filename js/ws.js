var WebSocketClient = require('websocket').client
var client = new WebSocketClient()
client.connect('ws://localhost:8000/ws')

const events = {
	subscribe: {
		name: "subscribe",
		payload: ""
	},
	unsubscribe: {
		name: "unsubscribe",
		payload: ""
	},
	commit: {
		name: "commit",
		payload: ""
	},
	message: {
		text: "",
		created: Date.now(),
		by: "",
		to: "",
		type: "text"
	}
}
const rooms = ["room.$default"]

let connectionID

const handlerSubscribe = (connection, rooms = []) => {
	let { subscribe } = events
	subscribe.payload = JSON.stringify(rooms)
	connection.send(JSON.stringify(subscribe))
}

const handlerUnsubscribe = (connection, rooms = []) => {
	let { unsubscribe } = events
	unsubscribe.payload = JSON.stringify(rooms)

	connection.send(JSON.stringify(unsubscribe))
}

const handlerCommit = (connection, message) => {
	let { commit } = events
	commit.payload = message.id
	connection.send(JSON.stringify(commit))
}

const handlerSender = (connection, text, room = []) => {
	let { message } = events
	const messagePayload = {
		name: "message",
		payload: JSON.stringify({
			...message,
			text,
			to: "room.$default",
		})
	}
	connection.send(JSON.stringify(messagePayload))
}

const handlerListener = (connection) => {
	connection.on('message', message => {
		console.log("Received: " + JSON.stringify(message))
	})
}


client.on('connect', connection => {
	console.log('WebSocket Client Connected');
	connection.on('error', error => {
		console.log("Connection Error: " + error.toString());
	})

	connection.on('close', _ => {
		console.log('echo-protocol Connection Closed');
	})

	handlerSubscribe(connection,rooms)
	handlerSender(connection, 'js sender' ,rooms[0])
	handlerListener(connection)

})